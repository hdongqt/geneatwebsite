import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class FooterComponent implements OnInit {
  constructor(private router : Router) { }

  ngOnInit(): void {
  }
  onScrollTop(){
    if(this.router.url == "/"){
      window.scrollTo({ top: 0, behavior: 'smooth' });
    }else{
      this.router.navigate(["/"])
    }
}

  
}
