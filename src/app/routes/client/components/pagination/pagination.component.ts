import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html'
})
export class PaginationComponent implements OnInit, OnChanges {
  @Input() pageSizeOptions: number[] = [];
  @Input() total = 1;
  @Input() pageSize = 3;
  @Input() pageIndex = 1;
  @Input() totalPages = 1;
  @Output() readonly pageIndexChange = new EventEmitter<number>();
  @Output() readonly queryParams = new EventEmitter();
  pageFromTo = {
    from: 0,
    to: 0
  }

  ranges: number[] = [];
  listOfPageItem: any[] = [];

  constructor() { }

  ngOnInit(): void {
    this.getPageFromTo()
  } 
  ngOnChanges(changes: SimpleChanges): void {
    this.getPageFromTo()
  }
  getPageFromTo(){
    let from = (this.pageIndex - 1) * this.pageSize + 1
    let to = this.pageSize * this.pageIndex 
    if(to > this.total) to = this.total
    this.pageFromTo = {from,to}
  }
  onPageIndexChange(index: number): void {
    this.pageIndex = index;
    this.pageIndexChange.emit(index);
  }

 

}
