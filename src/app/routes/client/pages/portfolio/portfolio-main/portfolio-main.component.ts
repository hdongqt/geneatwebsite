import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServicePortfolio } from '@services/api/portfolio.service';
import * as moment from 'moment';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';


@Component({
  selector: 'app-portfolio-main',
  templateUrl: './portfolio-main.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [ServicePortfolio]
})
export class  PortfolioMainComponent implements OnInit {
  postList: any[] = []
  constructor(
    public serviceportfolio: ServicePortfolio
  ) { }
  ngOnInit(): void {
    this.getListPortfolio();
  }
  getListPortfolio(){
    this.serviceportfolio.getPortfolio({
      page: 1,
      size: 9,
      filter: '{"slug":"portfolio"}' 
    }).subscribe((res:{data:any})=> {
      this.postList = res.data.content[0].postList
      console.log(res.data.content);
    })
  }
  formatDate(date: any, format = 'HH:mm DD/MM/YYYY'): string {
    return date ? moment(date).format(format) : '';
  }
}
