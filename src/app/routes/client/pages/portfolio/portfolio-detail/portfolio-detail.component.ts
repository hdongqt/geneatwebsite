import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { ServicePortfolio } from '@services/api/portfolio.service';
// @ts-ignore
import GLightbox from 'glightbox';
import * as moment from 'moment';

@Component({
  selector: 'app-portfolio-detail',
  templateUrl: './portfolio-detail.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [ServicePortfolio]
  
})
export class PortfolioDetailComponent implements OnInit {
    toggleNavMobile = true;
    postListDetail?: any[] = []
    constructor(
      private router: Router,
      public serviceportfolio: ServicePortfolio
    ) { }
    ngOnInit(): void {
      this.getListPortfolioDetail();
      setTimeout(() => GLightbox());
    }
    getListPortfolioDetail(){
      this.serviceportfolio.getListPortfolioDetail({
        page: 1,
        size: 9,
        filter: '{}' 
      }).subscribe((res:{data:any})=> {
        this.postListDetail = res.data.content
      })
    }
    formatDate(date: any, format = 'HH:mm DD/MM/YYYY'): string {
      return date ? moment(date).format(format) : '';
    }
    scrollToDetail(id:string){
      const yOffset = -80; 
      const el = document.querySelector(id);
      if(el!=null){
        const y = el.getBoundingClientRect().top + window.pageYOffset + yOffset;
        window.scrollTo({top: y, behavior: 'smooth'});
      }
    }
    scrollToSectionHome(id:string){
      this.router.navigate(["/"])
      setTimeout(()=>{
        document.querySelector(id)?.scrollIntoView({ behavior: 'smooth' });
      })
  }
}

