import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {map, take} from 'rxjs/operators';

import {ServiceAuth} from "@services/api/auth.service";

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(private serviceAuth: ServiceAuth, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return this.serviceAuth.user.pipe(
      take(1),
      map(user => {
        if (!user.token) {
          this.serviceAuth.isShowModalAuth$.next(true);
          this.serviceAuth.isShowLogin$.next(true);
        }
        return !!user.token;
      })
    );
  }
}
