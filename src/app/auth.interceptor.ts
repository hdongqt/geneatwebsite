import { Injectable } from '@angular/core';
import {HttpInterceptor, HttpRequest, HttpHandler, HttpEvent} from '@angular/common/http';
import {Observable} from 'rxjs';
import {exhaustMap, take} from 'rxjs/operators';

import { ServiceAuth } from '@services/api/auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private serviceAuth: ServiceAuth) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.serviceAuth.user.pipe(
      take(1),
      exhaustMap(user => {
        const setHeaders : {Authorization?: string, 'x-language': string, 'x-currency': string} = {
          'x-language': localStorage.getItem('ng-language') || 'en',
          'x-currency': localStorage.getItem('x-currency') || 'USD',
        };
        if (!req.headers.get('Authorization')) {
          setHeaders['Authorization'] = 'Bearer ' + (user ? user.token : '');
        }
        return next.handle(req.clone({setHeaders}));
      }),
    );
  }
}
