import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, Validators,FormGroup,FormControl } from '@angular/forms';
import {BaseForm} from "@routes/client/base/form";
import { getISOWeek } from 'date-fns';
import { ServiceEnquiry } from '@services/api/enquiries.service';
import { ServiceCountries } from '@services/api/countries.service';
export const moment: typeof import('moment') = window['moment'];
@Component({
  selector: 'app-enquiry',
  templateUrl: './enquiry.component.html',
  providers: [ServiceEnquiry,ServiceCountries]
})
export class EnquiryComponent extends BaseForm implements OnInit {
  date = null;
  form!: FormGroup;
  public success = false;
  listCountries:any[] = [];
  public enquiryInfo:{
    demand?: string,
    yourTitle?: string,
    name?: string,
    phoneNumber?: string,
    emailAddress?: string,
    selectionContact?: string,
    timeContact?: string,
    countryCode?: string,
    additionalQuestions?: string
  } = {};

  constructor(
    private fb : FormBuilder,
    public serviceEnquiry: ServiceEnquiry,
    public serviceCountry : ServiceCountries,
    ) {
    super();
    this.buildForm();
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      demand: [null, Validators.required],
      yourTitle: [null, Validators.required],
      name: [null, Validators.required],
      phoneNumber: [null,  [Validators.required, Validators.pattern(/^-?(0|[0-9]{6,20}\d*)?$/),Validators.minLength(6) ]],
      emailAddress: [null,[Validators.required, this.emailValidator]],
      selectionContact: [null],
      timeContact: [null,this.timeContactValidator],
      countryCode: [null,Validators.required],
      additionalQuestions:[null,Validators.required]
    });
    this.getCountry();
  }
  handleValid() {
    const { value, valid, controls } = this.form;
    if (valid) {
      this.enquiryInfo = {...this.enquiryInfo, ...value};
      this.submitFormEnquiry(this.enquiryInfo);
      this.success = true;
    } else {
      Object.values(controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }
  getCountry(){
    this.serviceCountry.getCountries({
      page: -1,
      size: 20,
      filter: '{}' 
    }).subscribe((res: any)=> {
       this.listCountries =  res.data.content
      })
  }
  submitFormEnquiry(data:any){
    this.serviceEnquiry.postEnquiry(data).subscribe((res:any)=>{
      this.form.reset();
    });
  }
  
  emailValidator(control: FormControl): { [s: string]: boolean } {
    if (control.value && !/^[A-z0-9._%+-]+@[A-z0-9.-]+\.[A-z]{1,4}$/.test(control.value)) {
        return { 'email': true }
    }
    return {};
  }
  timeContactValidator(date: FormControl,format = 'YYYY-MM-DD'): { [s: string]: boolean } {
    if(date.value){
      const dn = moment().format(format)
      const d = moment(date.value).format(format) 
     if(dn>d){
       return { 'timeContact': true }
      }else {
        return {}
     }
    }else return {};
  }

  private buildForm() {
    this.messagesErrorForm = {
      demand: {
        required: 'Please enter this field',
      },
      yourTitle: {
        required: 'Please enter this field',
      },
      name: {
        required: 'Please enter this field',
      },
      phoneNumber:{
        pattern: ['Invalid phone number'],
        minlength: ['Invalid phone number'],
        required: ['Please enter this field']
      },
      emailAddress: {
        required: ['Please enter this field'],
        email: ['Invalid email'],
      },
      countryCode: {
        required: 'Please enter this field',
      },
      timeContact: {
        timeContact: ["Please choose a date greater than the current date"]
      },
      additionalQuestions:{
        required: 'Please enter this field',
      }
    }

  }
  
}
