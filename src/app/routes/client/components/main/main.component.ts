import {AfterViewInit, Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class MainComponent implements OnInit, OnDestroy {
  constructor(
    protected route: ActivatedRoute,
    protected translateService: TranslateService,
  ) { }
 

  ngOnInit(): void {
    this.translateService.onDefaultLangChange.subscribe((langChangeEvent: LangChangeEvent) => {
      document.body.className = langChangeEvent.lang;
      this.hideBtnScrollToTop();
    });
  }

  hideBtnScrollToTop(){
    window.addEventListener('scroll',(event) => {
      if(scrollY == 0){ 
     document.querySelector("#to-top")?.classList.add("hidden");
      } else {
        document.querySelector("#to-top")?.classList.remove("hidden");
      }
    });
  }
  scrollToTop(){
      window.scrollTo({ top: 0, behavior: 'smooth' });
  }

  ngOnDestroy(): void {
  }
}
