import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {throwError} from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { environment } from '@src/environments/environment';
import {ServiceMessage} from "@services/message.service";

@Injectable()
export class ServicePortfolio {
  constructor(
    private http: HttpClient,
    protected message: ServiceMessage,
    private Router: Router
  ) {
  }
  getPortfolio(params: {page: number, size: number, filter: string, sort?: string}): any {
    return this.http
    .get<any>(environment.adminApiUrl + '/api/v1/post-categories', { params })
    .pipe(
      catchError(err => this.handleError(err)),
    );
  }
  getListPortfolioDetail(params: {page: number, size: number, filter: string, sort?: string}): any {
    return this.http
    .get<any>(environment.adminApiUrl + '/api/v1/posts', { params })
    .pipe(
      catchError(err => this.handleError(err)),
    );
  }

  handleError(err: HttpErrorResponse): any {
    if (err.error.message) {
      this.message.error(err.error.message);
    }
    return throwError(err.error);
  }

}