export const environment = {
  production: true,
  userData: '8b046762-d226-43ef-b925-bc49f57a975e',
  adminApiUrl: 'http://dev.geneat.vn:8200',
  oldApiUrl: 'https://api.inmergers.com',
  partnerId: '32112b98-59fe-43b6-bf27-ce3fc4344a5a',
  googleAuthId: '1027957143194-7772rg9nthesnjqkdd9qmthci5s5iffd.apps.googleusercontent.com',
  status: 'production'
};
