import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-service-main',
  templateUrl: './service-main.component.html'
})
export class ServiceMainComponent implements OnInit {



  listBusinessPlan = 
  [
    {
      iconClass:"las la-sliders-h la-2x",
      title:"IT Consultancy",
      content:"We believe brand interaction is key to communication. Real innovations and positive customer experience are the heart of success."
    },
    {
      iconClass:"las la-briefcase la-2x",
      title:"Web Development",
      content:"We believe brand interaction is key to communication. Real innovations and positive customer experience are the heart of success."
    },
    {
      iconClass:"las la-chart-bar la-2x",
      title:"App Development",
      content:"We believe brand interaction is key to communication. Real innovations and positive customer experience are the heart of success."
    },
    {
      iconClass:"las la-chart-pie la-2x",
      title:"Digital Marketing",
      content:"We believe brand interaction is key to communication. Real innovations and positive customer experience are the heart of success."
    },
    {
      iconClass:"las la-at la-2x",
      title:"Right Solutions",
      content:"We believe brand interaction is key to communication. Real innovations and positive customer experience are the heart of success."
    },
    {
      iconClass:"las la-desktop la-2x",
      title:"Responsive Site",
      content:"We believe brand interaction is key to communication. Real innovations and positive customer experience are the heart of success."
    },
    {
      iconClass:"las la-comments la-2x",
      title:"Clean Design",
      content:"We believe brand interaction is key to communication. Real innovations and positive customer experience are the heart of success."
    },
    {
      iconClass:"las la-tags la-2x",
      title:"Process Research",
      content:"We believe brand interaction is key to communication. Real innovations and positive customer experience are the heart of success."
    }
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
