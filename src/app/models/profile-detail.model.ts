export interface ProfileDetail {
  fullName?:                               string;
  isAfterConnectDataAvailable?:            boolean;
  companyName?:                            string;
  isDisplayContactDetails?:                boolean;
  isDisplayCompanyDetails?:                boolean;
  establishedYear?:                        number;
  establishedYearString?:                  string;
  lastAnnualSalesString?:                  string;
  profitMarginPercentageString?:           string;
  locationString?:                         string;
  locationListCode?:                       string[];
  locationList?:                           LocationList[];
  oldIndustryList?:                        oldIndustryList[];
  employeeCount?:                          number;
  employeeCountString?:                    string;
  productAndServiceDescription?:           string;
  lastAnnualSales?:                        number;
  isDisplayAnnualSalesAsRange?:            boolean;
  isDisplayProfitMarginPercentageAsRange?: boolean;
  physicalAssetsValue?:                    number;
  expectedSellingPrice?:                   number;
  isAllowBrokerCommunication?:             boolean;
  maximumStakePercentage?:                 number;
  investmentAmount?:                       number;
  legalEntityTypeCode?:                    null;
  businessRoleCode?:                       string;
  businessRole?:                           BusinessRole;
  sellerDemandCode?:                       string;
  sellerDemand?:                           BusinessRole;
  originallyPurchasedYear?:                string;
  priceCalculationMethod?:                 null;
  sellingPriceAmount?:                     number;
  saleAssets?:                             null;
  profitMarginPercentage?:                 number;
  oldIndustryListCode?:                    null;
  phoneNumber?:                            null;
  isPhoneNumberVerified?:                  boolean;
  email?:                                  string;
  isEmailVerified?:                        boolean;
  industryString?:                         string;
  industryListCode?:                       string[];
  industryList?:                           BusinessRole[];
  countryCode?:                            string;
  country?:                                Country;
  thumbnailUrl?:                           string;
  galleryListUrl?:                         string[];
  id:                                      string;
  title?:                                  string;
  shortDescription?:                       string;
  type?:                                   string;
  currency?:                               any;
  isPurchased?:                            boolean;
  totalPageViews?:                         number;
  totalInvestorViews?:                     number;
  scoreProfileCompleteness?:               number;
  scoreProfileStrength?:                   number;
  scoreProfileResponsiveness?:             number;
  scoreValuationFeedback?:                 number;
  scoreValuationAttractiveness?:           number;
  rating?:                                 number;
  statusCode?:                                 string;
  servicePackageId?:                       null;
  package?:                                ServicePackage;
  createdByUserId?:                        string;
  lastModifiedByUserId?:                   string;
  lastModifiedOnDate?:                     Date;
  createdOnDate?:                          Date;
  createdByUserName?:                      null;
  lastModifiedByUserName?:                 null;
  currentUserConnectStatus?: {
    isConnectAvailable: boolean,
    isConnected: boolean,
    isBookmarked : boolean,
    allowedActions: string[],
  };
}
export interface BuyerProfileDetail{
  id:                            string;
  fullName?:                     string;
  isAfterConnectDataAvailable?:  boolean;
  investmentRangeFrom?:          number;
  investmentRangeTo?:            number;
  websiteLinkedin?:              string;
  companyInformation?:           null;
  designation?:                  string;
  companySector?:                string;
  factors?:                      string;
  aboutCompany?:                 string;
  locationString?:               string;
  locationShortString?:          string;
  interestedLocationString?:     string[];
  locationList?:                 LocationList[];
  industryShortString?:          string;
  industryString?:               string;
  ghh?:     string;
  interestedLocationListCode?:   any[];
  interestedLocationList?:       any[];
  investorRoleCode?:             string;
  investorRole?:                 {
    id?:    string;
    title?: string;
    code?:  string;
  };
  investmentTypeString?:         string;
  investmentTypeListCode?:       string[];
  investmentTypeList?:           string[];
  currentLocationString?:        string;
  currentLocationListCode?:      any[];
  currentLocationList?:          any[];
  phoneNumber?:                  string;
  isPhoneNumberVerified?:        boolean;
  email?:                        string;
  isEmailVerified?:              boolean;
  title?:                        string;
  shortDescription?:             string;
  type?:                         string;
  currency?:                     any;
  isPurchased?:                  boolean;
  totalPageViews?:               number;
  totalInvestorViews?:           number;
  scoreProfileCompleteness?:     number;
  scoreProfileStrength?:         number;
  scoreProfileResponsiveness?:   number;
  scoreValuationFeedback?:       number;
  scoreValuationAttractiveness?: number;
  rating?:                       number;
  packageId?:                    null;
  package?:                      ServicePackage;
  statusCode?:                   null;
  status?:                       null;
  createdByUserId?:              string;
  lastModifiedByUserId?:         string;
  lastModifiedOnDate?:           Date;
  createdOnDate?:                Date;
  createdByUserName?:            string;
  lastModifiedByUserName?:       string;
  industryListCode?:             string[];
  industryList?:                 BusinessRole[];
  countryCode?:                  string;
  Country?:{
    name?:        string;
    iso3?:        string;
    iso2?:        string;
    currency?:    string;
    numericCode?: string;
    phoneCode?:   string;
    locationId?:  string;
    flagUrl?:     string;
  }
  currentUserConnectStatus?: {
    isConnectAvailable: boolean,
    isConnected: boolean,
    isBookmarked : boolean,
    allowedActions: string[],
  };
  thumbnailUrl?:                 string;
  galleryListUrl?:               string[];
}

export interface BusinessRole {
  id?:    string;
  title?: string;
  code?:  string;
}
export interface oldIndustryList {
  code?: string;
  id?: string;
  title?: string
}; 

export interface Country {
  name?:        string;
  iso3?:        string;
  iso2?:        string;
  currency?:    string;
  numericCode?: string;
  phoneCode?:   string;
  locationId?:  string;
}

export interface LocationList {
  name?: string;
  code?: string;
  type?: string;
}

export interface ServicePackage {
  id?:                        string;
  proposalIntroductionTotal?: number;
  isRecommended?:             boolean;
  totalMonthValidity?:        number;
  type?:                      number;
  isShow?:                    null;
  unlimitedProposal?:         boolean;
  name?:                      string;
  description?:               string;
  backgroundColor?:           string;
  badgeBackgroundColor?:      string;
  badgeForeColor?:            string;
  packagePrices?:             null;
  packageTranslations?:       null;
}
