import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServiceBlogs } from '@services/api/blogs.service';
export const moment: typeof import('moment') = window['moment'];

@Component({
  selector: 'app-blog-main',
  templateUrl: './blog-main.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [ServiceBlogs]
})
export class BlogMainComponent implements OnInit {
  categoryOfPost:any = {}
  public listBlogEvent: {
    content: any[],
    numberOfElements: number,
    page: number,
    size: number,
    totalElements: number,
    totalPages: number
  } = {
    content: [],
    size: 0,
    page: 0,
    totalElements: 0,
    totalPages: 0,
    numberOfElements: 0,
  };
   listBlogCategory: any[] = []

  constructor(
    public serviceblogs: ServiceBlogs,
    private _route: ActivatedRoute,
  ) {
  }
  ngOnInit(): void {
    this.getListNews();
    this.getListCategory();
  }
  getListNews(){
    const paramMap = this._route.snapshot.paramMap;
    const cateSlug = paramMap.get('cateSlug');
    if(cateSlug){
      this.serviceblogs.getListBlogByCategorys({
        page: 1,
        size: 3,
        filter: JSON.stringify({slug: cateSlug })
      }).subscribe((res:{data:any})=> {
        this.listBlogEvent = res.data;
        const content = res.data.content[0];
        this.listBlogEvent.content = content.postList
        const {title,slug,cover} = content
        this.categoryOfPost = {title,slug,cover}
      })
    }else{
      this.serviceblogs.getListBlogs({
        page: 1,
        size: 3,
        filter: JSON.stringify({Type: "BLOG",slug: cateSlug})
      }).subscribe((res:{data:any})=> {
        this.listBlogEvent = res.data;
      })
    }
  }
  getListCategory(){
    this.serviceblogs.getListBlogByCategorys({
      page: 1,
      size: 3,
      filter: JSON.stringify({Type: "BLOG"})
    }).subscribe((res:{data:any})=> {
      this.listBlogCategory = res.data.content;
    })
  }
  hanldNews({size = 3, page = 1}){
    this.serviceblogs.getListBlogs({
      page,
      size,
      filter: '{}'
    }).subscribe((res:{data:any})=>{
      this.listBlogEvent = res.data;
    })
  }
  formatDate(date: any, format = 'HH:mm DD/MM/YYYY'): string {
    return date ? moment(date).format(format) : '';
  }

}
