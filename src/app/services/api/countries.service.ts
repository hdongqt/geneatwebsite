import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {throwError} from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { environment } from '@src/environments/environment';
import {ServiceMessage} from "@services/message.service";

@Injectable()
export class ServiceCountries {
  constructor(
    private http: HttpClient,
    protected message: ServiceMessage,
  ) {
  }

  getCountries(params: {page: number, size: number, filter: string}):any{
    return this.http
    .get<any>(environment.adminApiUrl + '/api/v1/location/countries' ,{params})
    .pipe(
        catchError(err => this.handleError(err)),
    );
}
  handleError(err: HttpErrorResponse): any {
    if (err.error.message) {
      this.message.error(err.error.message);
    }
    return throwError(err.error);
  }

}
