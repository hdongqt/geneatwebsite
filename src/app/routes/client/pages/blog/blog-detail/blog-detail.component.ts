import { Component, OnInit } from '@angular/core';
import { ServiceBlogs } from '@services/api/blogs.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
// @ts-ignore
import GLightbox from 'glightbox';
export const moment: typeof import('moment') = window['moment'];


@Component({
  selector: 'app-blog-detail',
  templateUrl: './blog-detail.component.html',
  providers: [ServiceBlogs]
})
export class BlogDetailComponent implements OnInit {
  public blogDetail?: any;
  constructor(
    public serviceblogs: ServiceBlogs,
    private router: Router,
    private _route: ActivatedRoute,
    private titleService: Title
    ) { }
    
    ngOnInit(): void {
      setTimeout(() => GLightbox());
      const paramMap = this._route.snapshot.paramMap;
      this.titleService.setTitle("blog detail");

      const slug = paramMap.get('postSlug');
     if(slug){
      this.getBlogDetail(slug)
     }
  }


  getBlogDetail(blogSlug:string){
    if(blogSlug){
      this.serviceblogs.getBlogDetail(blogSlug)
      .subscribe((res: any) => {
        if(res.data){
          this.blogDetail = res.data;
        }
    });
    }
  }

  scrollToDetail(id:string){
    const yOffset = -80; 
    const el = document.querySelector(id);
    if(el!=null){
      const y = el.getBoundingClientRect().top + window.pageYOffset + yOffset;
      window.scrollTo({top: y, behavior: 'smooth'});
    }
  }
  formatDate(date: any, format = 'HH:mm DD/MM/YYYY'): string {
    return date ? moment(date).format(format) : '';
  }
  ngOnDestroy(): void {
    this.titleService.setTitle("GENEAT SOFTWARE - High performance software systems ");
  }
}
