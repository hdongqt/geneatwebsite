import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, Validators,FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import {BaseForm} from "@routes/client/base/form";
import { ServiceContact } from '@services/api/contact.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  providers: [ServiceContact]
  })
export class HomeComponent extends BaseForm implements OnInit {
  form!: FormGroup;
  public success = false;
  public contactInfo:{
    infoSender?: string,
    content?: string,
  } = {};
  listService= [
    {
      title: "Web Apps Development",
      img: "assets/images/home/service_1.jpg",
      slug : "mobile-development"
  },
  {
      title: "Mobile Application Development",
      img: "assets/images/home/service_2.jpg",
      slug : "software-development"
  },
  {
      title: "Software System Maintenance",
      img: "assets/images/home/service_3.jpg",
      slug : "system-maintenance"
  }
];
listAccomplishment = [
  {
      img: "assets/images/home/dcs-cover-thumb.jpg",
      sub: "Multiplatform sales network.",
      title: "DCS",
      slug: "/portfolio/dcs"
  },
  {
      img: "assets/images/home/cms-cover-thumb.jpg",
      sub: "CMSContractor Website.",
      title: "CMS Contractor",
      slug: "/portfolio/like-zone-sales-system"
  },
  {
      img: "assets/images/home/scn-cover-thumb.jpg",
      sub: "Collaborator network",
      title: "SCN",
      slug: "/portfolio/scn"
  },
  {
    img: "assets/images/home/stup-cover-thumb.5c1e01e.aec396f6a8d277d220f99203ad6e96ec.jpg",
    sub: "A startup for financial",
    title: "Stup.vn",
    slug: "/portfolio/stup"
  },
  {
    img: "assets/images/home/webexaminer-cover-thumb.5c1e01e.a6ec73c614d7cd85e916371a80e4a7b3.jpg",
    sub: "Website for WebExaminer",
    title: "Webexaminer",
    slug: "/portfolio/web-examiner"
  }
]

listBusiness = [
  {
    img: "assets/images/home/gioi-thieu-phan-mem-can-xe-dien-tu-thumb.jpg",
    title: "Difficulties in selling online and advantages in selling through apps",
    content: "Advantages of selling through apps",
      slug: "/blog/geneatscn/mo-hinh-ctv-kho-khan-trong-ban-hang-online-va-uu-diem-khi-ban-hang-qua-ung-dung-app/"
  },
  {
    img: "assets/images/home/ban-hang-theo-mang-luoi-CTV-xu-huong-cho-nhung-shop-lon-thumb.jpg",
    title: "  Sales online trends for big shops",
    content: "The current trend of collaborator network sales",
      slug: "/blog/geneatscn/ban-hang-theo-mang-luoi-ctv-xu-huong-cho-nhung-shop-lon/"
  },
  {
    img: "assets/images/home/cac-loai-dau-can-xe-pho-bien-thumb.jpg",
    title: "Benefits of the GENEAT SCN platform for Shops",
    content: "Overview of the Geneat SCN platform and its benefits",
      slug: "/blog/geneatscn/loi-ich-cua-nen-tang-geneat-scn-doi-voi-shop-khi-ban-hang-theo-mang-luoi-ctv/"
  }
]

listClient = [
  {
    img:"assets/images/home/person_1.jpg",
    name:"Mr. Robin Appleton-Power",
    title:"Director - WebExaminer",
    content:"Possessing expert knowledge of .NET, web services and DEVOPS, GENEAT has a flair for putting together a system design on all levels."

  },
  {
    img:"assets/images/home/person_2.jpg",
    name:"Mr. Ninh Thanh Trung",
    title:"Director - NTR Corp",
    content:"Your consulting process impress me a lot. Clean and direct, putting customer benefit first. You made me very happy with my investment."

  },
  {
    img:"assets/images/home/person_3.jpg",
    name:"Mr. Anh Tuan Le",
    title:"Manager - ZoneGroup",
    content:"Our business was totally enhanced after cooperate with GENEAT. The management of my store chain become more effective and easy."

  },
  {
    img:"assets/images/home/person_4.jpg",
    name:"Mr. Tung Hoang",
    title:"CEO - Stup.vn",
    content:"GENEAT has a professional working process, good quality products but very reasonable costs. My recommendation for any startups."

  },
  {
    img:"assets/images/home/person_5.jpg",
    name:"Ms. An Nguyen",
    title:"Founder - DCS Store",
    content:"The solution provided by GENEAT actually increased our sales by 500%, far exceeding our initial expectations. Great teamwork and collaboration."

  }
]
  constructor(
    private router : Router,
    private fb : FormBuilder,
    public serviceContact: ServiceContact,
    ) {
    super();
    this.buildForm();


  }

  ngOnInit(): void {
    this.form = this.fb.group({
      infoSender: [null, Validators.required],
      content:[null, Validators.required]
    });
  }
  handleValid() {
    const { value, valid, controls } = this.form;
    console.log(value)
    if (valid) {
      this.contactInfo = {...this.contactInfo, ...value};
      this.submitFormEnquiry(this.contactInfo);
      this.success = true;
    } else {
      Object.values(controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }
  submitFormEnquiry(data:any){
    this.serviceContact.postContact(data).subscribe((res:any)=>{
      this.form.reset();
    });
  }

  onScroll(data:string){

      document.querySelector(''+data)?.scrollIntoView({ behavior: 'smooth' });

    }
  private buildForm() {
    this.messagesErrorForm = {
      infoSender: {
        required: 'Please enter this field',
      },
      content: {
        required: 'Please enter this field',
      },
    }

  }
}

