// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  userData: '5e457b36-aa91-4272-ab04-942f82376e79',
  adminApiUrl: 'http://dev.geneat.vn:8200',
  oldApiUrl: 'https://api.inmergers.com',
  partnerId: 'fa41b121-f7b0-4e32-a129-d9ee8ac9348e',
  googleAuthId: '458714151950-fv7bp4u656lk3bel41agq6jh2j4b27pb.apps.googleusercontent.com',
  status: 'development'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
