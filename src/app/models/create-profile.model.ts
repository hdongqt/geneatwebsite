export interface SellerProfile{
    id?:                                     string;
      fullName?:                               string;
      companyName?:                            string;
      isDisplayContactDetails?:                boolean;
      isDisplayCompanyDetails?:                boolean;
      establishedYear?:                        number;
      locationListCode?:                       string[];
      employeeCount?:                          number;
      productAndServiceDescription?:           string;
      lastAnnualSales?:                        number;
      isDisplayAnnualSalesAsRange?:            boolean;
      isDisplayProfitMarginPercentageAsRange?: boolean;
      physicalAssetsValue?:                    number;
      expectedSellingPrice?:                   number;
      isAllowBrokerCommunication?:             boolean;
      maximumStakePercentage?:                 number;
      investmentAmount?:                       number;
      legalEntityTypeCode?:                    string;
      businessRoleCode?:                       string;
      sellerDemandCode?:                       string;
      originallyPurchasedYear?:                string;
      priceCalculationMethod?:                 string;
      sellingPriceAmount?:                     number;
      saleAssets?:                             string;
      profitMarginPercentage?:                 number;
      oldIndustryListCode?:                    string[];
      title?:                                  string;
      isPurchased?:                            boolean;
      shortDescription?:                       string;
      phoneNumber?:                            string;
      isPhoneNumberVerified?:                  boolean;
      email?:                                  string;
      isEmailVerified?:                        boolean;
      industryListCode?:                       string;
      countryCode?:                            string;
      currency?:                               string;
      country?:{
        name?:string;
        locationId?:string;
      }
      package?:{
        name?:string;
        packagePrices?:number;
      }
      statusCode?: string;
}


export interface FranchiseProfile {
    id?:                                 string;
    fullName?:                           string;
    companyName?:                        string;
    website?:                            string;
    companyDescription?:                 string;
    productDescription?:                 string;
    startOperationYear?:                 number;
    designation?:                        string;
    totalGlobalOutlets?:                 number;
    oemEmployeeCount?:                   number;
    oemAverageMontlyOutput?:             number;
    oemWellknownCustomers?:              string;
    expandLocationListCode?:             string;
    headquarterLocationCode?:            string;
    salesPartnerFormatCount?:            number;
    ownerExpectation?: string;
    brandFeeAmount?:                     number;
    minimumStaffCount?:                  number;
    franchiseTypeCode?:                  string;
    ownerSupport?:                       string;
    title?:                              string;
    shortDescription?:                   string;
    phoneNumber?:                        string;
    isPhoneNumberVerified?:              boolean;
    email?:                              string;
    isEmailVerified?:                    boolean;
    industryListCode?:                   string[];
    countryCode?:                        string;
    currency?:                           string;
    package?:{
      name?:string,
      packagePrices?:number,
    }
    franchiseFormats?: {
      id?:                           string;
      name?:                         string;
      minimumSpaceArea?:             number;
      minimumInvestmentAmount?:      number;
      minimumStaffCount?:            number;
      brandFeeAmount?:               number;
      averageMonthlySalesPerDealer?: number;
      profitMarginPercentage?:       number;
      loyaltyCommisionDetails?:      string;
    }[];
    statusCode?: string;
}

export interface BuyerProfile {
    id?:                         string;
    fullName?:                   string;
    investmentRangeFrom?:        number;
    investmentRangeTo?:          number;
    currentLocationListCode?:    string;
    websiteLinkedin?:            string;
    companyInformation?:         string;
    designation?:                string;
    companySector?:              string;
    factors?:                    string;
    aboutCompany?:               string;
    investorRoleCode?:           string;
    investmentTypeListCode?:     string[];
    interestedLocationListCode?: string;
    isPurchased?:                boolean;
    industryListCode?:           string[];
    title?:                      string;
    shortDescription?:           string;
    phoneNumber?:                string;
    isPhoneNumberVerified?:      boolean;
    email?:                      string;
    isEmailVerified?:            boolean;
    countryCode?:                string;
    currency?:                   string;
    statusCode?: string;
}