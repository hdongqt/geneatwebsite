import { Directive, Input, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({
  selector: '[tooltip]'
})
export class TooltipDirective {
  @Input('tooltip') title!: string;
  @Input() placement = 'top';
  tooltip!: HTMLElement | null;

  constructor(private el: ElementRef, private renderer: Renderer2) { }

  @HostListener('mouseenter') onMouseEnter() {
    if (!this.tooltip) {
      this.create();
      this.setPosition();
    }
  }

  @HostListener('mouseleave') onMouseLeave() {
    if (this.tooltip) {
      this.tooltip.className = 'tooltip tooltip-' + this.placement;

      window.setTimeout(() => {
        document.body.removeChild(this.tooltip!);
        this.tooltip = null;
      }, 150);
    }
  }

  create() {
    document.querySelectorAll(".tooltip-show").forEach(el => el.remove());
    this.tooltip = this.renderer.createElement('span');
    if (this.tooltip) {
      this.tooltip.className = 'tooltip tooltip-show tooltip-' + this.placement;
      this.tooltip.innerHTML = this.title;
      document.body.appendChild(this.tooltip)
    }
  }

  setPosition() {
    const hostPos = this.el.nativeElement.getBoundingClientRect();
    if (this.tooltip) {
      const tooltipPos = this.tooltip.getBoundingClientRect();

      const scrollPos = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;

      let top, left;

      switch (this.placement) {
        case 'right':
          top = hostPos.top + (hostPos.height - tooltipPos.height) / 2;
          left = hostPos.right + 10;
          break;
        case 'left':
          top = hostPos.top + (hostPos.height - tooltipPos.height) / 2;
          left = hostPos.left - tooltipPos.width - 10;
          break;
        case 'bottom':
          top = hostPos.bottom + 10;
          left = hostPos.left + (hostPos.width - tooltipPos.width) / 2;
          break;
        case 'top':
        default:
          top = hostPos.top - tooltipPos.height - 10;
          left = hostPos.left + (hostPos.width - tooltipPos.width) / 2;
      }

      this.tooltip.style.top = `${top + scrollPos}px`;
      this.tooltip.style.left = `${left}px`;
    }
  }
}
