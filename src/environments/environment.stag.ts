export const environment = {
  production: true,
  userData: '5e457b36-aa91-4272-ab04-942f82376e79',
  adminApiUrl: 'http://dev.geneat.vn:8200',
  oldApiUrl: 'https://api.inmergers.com',
  partnerId: 'fa41b121-f7b0-4e32-a129-d9ee8ac9348e',
  googleAuthId: '458714151950-fv7bp4u656lk3bel41agq6jh2j4b27pb.apps.googleusercontent.com',
  status: 'staging'
};
