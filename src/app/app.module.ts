import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { registerLocaleData, LocationStrategy, PathLocationStrategy,CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import en from '@angular/common/locales/en';
import {AuthInterceptor} from "@src/app/auth.interceptor";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
registerLocaleData(en);
import {HttpClientModule} from "@angular/common/http";
import { en_US, NZ_I18N } from 'ng-zorro-antd/i18n';
import { TranslateModule , TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import {ConvertCurrencyPipe} from "@src/app/pipe/convert-currency.pipe";
import {FullTextSearchPipe} from "@src/app/pipe/full-text-search.pipe";
import {HeaderComponent} from "@routes/client/components/header/header.component";
import {FooterComponent} from "@routes/client/components/footer/footer.component";
import {MainComponent} from "@routes/client/components/main/main.component";
import {HomeComponent} from "@routes/client/pages/home/home.component";
import {SharedDirective} from "@src/app/directive/shared.directive";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NzSelectModule} from "ng-zorro-antd/select";
import {NzDropDownModule} from "ng-zorro-antd/dropdown";
import {NzTabsModule} from "ng-zorro-antd/tabs";
import {NzRadioModule} from "ng-zorro-antd/radio";
import {NzSliderModule} from "ng-zorro-antd/slider";
import {NzSwitchModule} from "ng-zorro-antd/switch";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzTimePickerModule} from "ng-zorro-antd/time-picker";
import {NzDatePickerModule} from "ng-zorro-antd/date-picker";

import { ServiceMainComponent } from './routes/client/pages/service/service-main/service-main.component';
import { ServiceDetailComponent } from './routes/client/pages/service/service-detail/service-detail.component';
import { BlogMainComponent } from './routes/client/pages/blog/blog-main/blog-main.component';
import { BlogDetailComponent } from './routes/client/pages/blog/blog-detail/blog-detail.component';
import { PaginationComponent } from './routes/client/components/pagination/pagination.component';
import { ServiceCarouselComponent } from './routes/client/components/service-carousel/service-carousel.component';
import { PortfolioDetailComponent } from './routes/client/pages/portfolio/portfolio-detail/portfolio-detail.component';
import { PortfolioMainComponent } from './routes/client/pages/portfolio/portfolio-main/portfolio-main.component';
import { EnquiryComponent } from './routes/client/pages/enquiry/enquiry.component';
export function HttpLoaderFactory(http : HttpClient){
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    ConvertCurrencyPipe,
    FullTextSearchPipe,
    HeaderComponent,
    FooterComponent,
    MainComponent,
    HomeComponent,
    ServiceMainComponent,
    ServiceDetailComponent,
    BlogMainComponent,
    BlogDetailComponent,
    PaginationComponent,
    ServiceCarouselComponent,
    PortfolioDetailComponent,
    PortfolioMainComponent,
    EnquiryComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NzToolTipModule,
    TranslateModule.forRoot(
      {
        loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
        }
      }
      ),
      SharedDirective,
      CommonModule,
      TranslateModule.forRoot({
        defaultLanguage: localStorage.getItem('ng-language') || 'en',
        loader: {
          provide: TranslateLoader,
          useFactory: function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
            return new TranslateHttpLoader(http, 'assets/translations/');
          },
          deps: [HttpClient]
        }
      }),
      FormsModule,
      NzSelectModule,
      NzDropDownModule,
      NzTabsModule,
      NzRadioModule,
      NzSliderModule,
      NzSwitchModule,
      NzCheckboxModule,
      NzFormModule,
      ReactiveFormsModule,
      NzTimePickerModule,
      NzDatePickerModule,
      NzToolTipModule
  
  ],
  providers: [
    {provide: LocationStrategy, useClass: PathLocationStrategy},
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    {provide: NZ_I18N, useValue: en_US}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
