import { Component } from '@angular/core';
import {ServiceAuth} from "@services/api/auth.service";

@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet>',
  providers: []
})
export class AppComponent {
  constructor(private serviceAuth: ServiceAuth) {
  }

  ngOnInit(): void {
    this.serviceAuth.autoLogin();
  }
}
