import {Component, OnInit, ViewEncapsulation,OnChanges, EventEmitter,Output} from '@angular/core';
import { ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [],
})

export class HeaderComponent implements OnInit {
  @ViewChild('scroll') scroll!: ElementRef;
  toggleNavMobile:boolean = false;
  
  listHeader = [
    {
      title: "Home",
      link: "",
      linkID: "home"
    },
    {
      title: "Our Service",
      link: "",
      linkID: "service-section"
    },
    {
      title: "Portfolio",
      link: "portfolio",
      linkID: ""
    },
    {
      title: "Blog",
      link: "blog",
      linkID: ""
    },
    {
      title: "About Us",
      link: "",
      linkID: "about-section"
    },
    {
      title: "Contact",
      link: "",
      linkID: "contact-section"
    }

] 
  constructor(private router : Router) {}
  ngOnChanges(){
      
  }
  ngOnInit(): void {
    window.addEventListener('scroll',(event) => {
      if(scrollY == 0){ 
       
        this.scroll.nativeElement.classList.remove('shadow-md');
      } else {
        this.scroll.nativeElement.classList.add('shadow-md');
        
      }
    });
  }
  toogle(){
    this.toggleNavMobile = !this.toggleNavMobile
  } 
  onScroll(id:string){
      if(this.router.url == "/"){
        if(id == "home")  window.scrollTo({ top: 0, behavior: 'smooth' });
        document.querySelector("#"+id)?.scrollIntoView({ behavior: 'smooth' });
      }else{
        this.router.navigate(["/"])
       setTimeout(()=>{
        document.querySelector("#"+id)?.scrollIntoView({ behavior: 'smooth' });
       })
        
      }
  }
}
