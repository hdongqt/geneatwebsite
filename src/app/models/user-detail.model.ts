export interface UserDetail {
  fullTextAddress?:           null;
  id?:                        string;
  userName?:                  string;
  name?:                      string;
  email?:                     null;
  avatarUrl?:                 null;
  phoneNumber?:               string;
  address?:                   string;
  company?:                   string;
  designation?:               string;
  gtsNumber?:                 string;
  identificationNumber?:      string;
  industryString?:            string;
  preferredIndustryListCode?: string[];
  preferredIndustryList?:     PreferredIndustryList[];
  countryCode?:               string;
  country?:                   Country;
  locationString?:            string;
  locationCode?:              string;
  location?:                  Location;
  preferredLocationList?:     Location[];
  preferredLocationListCode?: string[];
  timeZone?:                  string;
  currency?:                  null;
  language?:                  null;
  type?:                      number;
  recentPaymentString?:       string;
  birthdate?:                 null;
  lastActivityDate?:          Date;
  applicationId?:             string;
  level?:                     number;
  isLockedOut?:               boolean;
}

export interface Country {
  name:        string;
  iso3:        string;
  iso2:        string;
  currency:    string;
  numericCode: string;
  phoneCode:   string;
  locationId:  string;
}

export interface Location {
  name: string;
  code: string;
  type: string;
}

export interface PreferredIndustryList {
  id:    string;
  title: string;
  code:  string;
}
