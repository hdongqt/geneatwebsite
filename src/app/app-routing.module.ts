import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "@routes/client/pages/home/home.component";
import {MainComponent} from "@routes/client/components/main/main.component";
import { ServiceMainComponent } from '@routes/client/pages/service/service-main/service-main.component';
import { ServiceDetailComponent } from '@routes/client/pages/service/service-detail/service-detail.component';
import { BlogMainComponent } from '@routes/client/pages/blog/blog-main/blog-main.component';
import { BlogDetailComponent } from '@routes/client/pages/blog/blog-detail/blog-detail.component';
import { PortfolioMainComponent } from './routes/client/pages/portfolio/portfolio-main/portfolio-main.component';
import { PortfolioDetailComponent } from '@routes/client/pages/portfolio/portfolio-detail/portfolio-detail.component';
import { EnquiryComponent } from '@routes/client/pages/enquiry/enquiry.component';


const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      { path: '', component: HomeComponent },
      {
        path: 'service', 
        children: [
          {
            path: "",
            component: ServiceMainComponent
          },
          {
            path: ":slug",
            component: ServiceDetailComponent
          }
        ]
      },
      { path: 'blog', 
      children: [
        {
          path: "",
          component: BlogMainComponent
        },
        {
          path: ":cateSlug",
          component: BlogMainComponent
        },
        {
          path: ":cateSlug/:postSlug",
          component: BlogDetailComponent
        }
      ]
      },
      { 
        path: 'portfolio', 
        children: [
          {
            path: "",
            component: PortfolioMainComponent
          },
          {
            path: ":slug",
            component: PortfolioDetailComponent
          }
        ]
      },
      { 
        path: 'enquiry', 
        component: EnquiryComponent
      },
      // { path: '**', redirectTo: '', pathMatch: 'full' },
    ]

  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
 exports: [RouterModule]
})
export class AppRoutingModule { }
